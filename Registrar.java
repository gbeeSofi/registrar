import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;
import javax.swing.JOptionPane;


/*
 * 
 * Author: Greg Bee
 * 
 * Date: 2/18/2012
 * 
 * Description: This is the main point of entry for Western Governors University, KFT1 task4 class.
 * 				This program is a Student database. It allows for Creating, Reading, Updating,
 * 				Deleting and searching students. It uses object oriented methodologies to perform
 * 				these features.
 * 
 * 
 * Revision History:
 * 		
 * 				GLB Created			2/18/2012
 * 
 */
public class Registrar {

	static int studentId;
	static int totalNumberOfStudents = 100; // limited to 100 for testing
	

	/////////////////
	// main method
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				
				
		

				
				////////////////////////////////////////
				// build main objects, view and database
				final View view = new View();
				

				////////////////////////////////////////
				// explain to reviewer how to populate
				// the database with test data.
				JOptionPane.showMessageDialog(null, "" +
						"Welcome to Student Registrar!\n\n " +
						"  To create test data, use the Help Menu\n\n" +
							"    1st- Drop the Student Table\n" +
							"    2nd- Create the Student Table\n" +
							"    3rd- Populate the Student Table with mock data\n" +
							"    4th- Count the students in the table to validate\n\n"+
							"         PRESS OK to Continue");

				//////////////////////////////
				// build main view objects
				view.buildWindow();
				view.buildMenuBar();
				view.buildSearchPanel();
				view.buildStudentPanel();
			
				
				
				/////////////////////////////////////////////////////////////////
				// Handle Events, search, forward, backward, delete update etc...
				
				
				////////////////////////////////
				// Read Students Back Button
				view.btnBackRead.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {				
											
					if(studentId == 1){
							view.btnBackRead.setEnabled(false);
							JOptionPane.showMessageDialog(null, "Start of Student Records");
						}else{
							view.btnForwardRead.setEnabled(true);
							Student bStudent =	DAO.getStudent(DAO.connectToDatabase(), --studentId);
							view.displayStudent(bStudent);
							while(bStudent == null){
								bStudent =	DAO.getStudent(DAO.connectToDatabase(), --studentId);
								view.displayStudent(bStudent);
							}
						}
					}
				});

				
				
				////////////////////////////////
				// Delete Student Delete Button
				view.btnDelete.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						Student studentToDelete = new Undergraduate();
						studentToDelete.setStudentId(view.getStudentId());				
						DAO.deleteStudent(studentToDelete);
					}
				});

				
				////////////////////////////////
				// Update Student Update Button
				view.btnUpdate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						Student theStudent = view.getStudentFromView();
						
						if(theStudent instanceof Undergraduate){
							JOptionPane.showMessageDialog(null, "Undergraduate Update");
							Undergraduate theUndergraduate= (Undergraduate) view.getStudentFromView();
							theUndergraduate.update();
						}
						if(theStudent instanceof Graduate){
							JOptionPane.showMessageDialog(null, "Graduate Update");
							Graduate theGraduate = (Graduate) view.getStudentFromView();
							theGraduate.update();
						}
						if(theStudent instanceof PartTime){
							JOptionPane.showMessageDialog(null, "PartTime Update");
							PartTime theParttime = (PartTime) view.getStudentFromView();
							theParttime.update();
						}
					}
				});
				
				
				////////////////////////////////
				// Read Students Forward Button
				view.btnForwardRead.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(studentId <totalNumberOfStudents){
							view.btnBackRead.setEnabled(true);
							Student fStudent =	DAO.getStudent(DAO.connectToDatabase(), ++studentId);
							view.displayStudent(fStudent);
							while(fStudent == null){
								fStudent =	DAO.getStudent(DAO.connectToDatabase(), ++studentId);
								view.displayStudent(fStudent);
							}
							
						}else{
							view.btnForwardRead.setEnabled(false);
							JOptionPane.showMessageDialog(null, "End of Student Records");
						}
					}
				});

				

				
				/////////////////////////
				// Search for a Student
				view.btnSearch.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Integer searchId = null;
						boolean byName= false;
						
						if ((view.getSearchStudentId().length() != 0)|| (view.getSearchFirstName().length()!=0) || (view.getSearchLastName().length()!=0)) {
						String temp1 = view.getSearchStudentId();
						if(temp1.length()>0){
							searchId = new Integer(temp1);
							studentId = searchId.intValue();
							byName = false;
						}else{
							byName = true;
						}
						
						Student searchStudent = new Undergraduate();
							
							if (byName == false) {
								searchStudent = DAO.getStudent(DAO.connectToDatabase(), studentId);
								view.displayStudent(searchStudent);
								if(searchId !=null){
									studentId = searchId.intValue();
								}
							} else {
								
								searchStudent.setFirstName(view.getSearchFirstName().toString());
								searchStudent.setLastName(view.getSearchLastName().toString());
								DAO.searchStudent(DAO.connectToDatabase(),searchStudent, view, byName);
							}
						}else{
							JOptionPane.showMessageDialog(null, "Please enter search criteria!");
						}
					}
				});
										
				
				////////////////////////////////////////
				// Create a New Undergraduate Student
				view.undergraduateStudentMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {					
						Undergraduate aNewUndergraduateStudent = new Undergraduate();
						View.newStudentDialog(aNewUndergraduateStudent);
					}
				});
				
				
				////////////////////////////////////////
				// Create a New Graduate Student
				view.graduateStudentMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Graduate graduateStudent = new Graduate();
						View.newStudentDialog(graduateStudent);
					}
				});

				
				////////////////////////////////////////
				// Create a New Part-time Student
				view.parttimeStudentMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						PartTime aNewPartTime = new PartTime();
						View.newStudentDialog(aNewPartTime);
					}
				});
			}
		});
	}
}
