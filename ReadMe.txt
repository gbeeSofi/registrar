
 


Installation Design and Use Instructions
________________________________________________________________
Student Registrar 1.0









Project Manager: Greg Bee
Development : Greg Bee
Quality Assurance : Greg Bee
Department: Student Registrar
Prepared By: Greg Bee
Student Id: 229575

 
Methodology / Tools
________________________________________________________________
Registrar is a simple client server application created in java using the java Swing user interface API and connecting to a MySQL database (version 5.5) via JDBC. Registrar is used to manage student records. The application can generate random fictitious students for testing.
It was created by Greg Bee for Western Governors Universities KFT1 Task 4 (Object Oriented Development and Design). 
The application does not use the java Swing layout managers, rather by passing null to the layout manager, the layout is controlled with absolute positioning (this provided more control to place the controls in locations that met my goal for the look of the application), as seen below:

 

This application provides the standard CRUDs (create, read, update, delete, search) operations typical of a relational database.

	Create, New Students (Undergraduate, Graduate, Part Time) (insert)
Read, Navigate the dataset with forward and backward buttons (select)
Update, modify existing student data (update)
Delete, Remove student records (delete)
Search, Locate students via student id or First, Last Name uses (select)
Search also displays a list of student if there are multiple matches.

This application was created in the Eclipse IDE. Version: Indigo Service Release 1, Build id: 20110916-0149

The final version delivered to task stream was compiled outside of Eclipse at a command line using:
javac *.java  

I added the mysql jdbc driver to the environment variables to operate outside of eclipse (Advanced System Settings).
I have included the jdbc driver (in my zip file/src dir). 

Below are the steps to deploy/install and launch this application.



Build/Deploy/Launch
________________________________________________________________
1-	Unzip Registrar.zip to your C Drive
2-	Add to your environment variable CLASSPATH: C:\Registrar\src\mysql.jar.
a.	For example mine looks like this:
i.	.;C:\Program Files (x86)\Java\jre6\lib\ext\QTJava.zip;C:\Registrar\src\mysql.jar

3-	Open a command line and navigate to C:\Registrar\src\
a.	Type
i.	javac *.java
ii.	java Registrar
1.	This will launch the application
4-	You will first see a reminder to populate the database with test data, the application assumes you have created a mySQL database, you will need to change the password at the top of the page DAO.java

 
a.	From the Help Menu
i.	Drop the Student Table
ii.	Create the Student Table
iii. Populate the Student Table
1.	This generates random data of 3 types of students
iv.	Check the count of the database, it should say 100.

 




Use
________________________________________________________________
After following the previous instructions, you will have 100 fictitious students populated in the Registrar database. You can select the forward button and backward button to navigate through the data. The delete button will remove the record currently displayed in the lower section. The Update button, updates the fields in the lower section of the application. You can Search for a Student via Student ID or First or Last Name. To create new Students use the Menu File New Student menu items for each type of student. You can also use Alt F1, F2 or F3 to open the new student create dialog. 

 Tuition Calculations
The tuition is calculated from the menu item under Tuition calculate, it calculates the tuition of the student currently displayed in the student area. The application assumes that Undergraduate students are taking 15 credit hours, Graduate students are taking 12 credit hours, and Part-Time students are taking 8 credit hours. This will result in the following calculations:

Undergraduate resident	15 x200 = $3000
Undergraduate non-resident  15x400= $6000
Graduate resident 12x300= $3600
Graduate non-resident  12x350=$4200
Part-time resident 8x250=2000
Part-time nonresident 8x450= $3600
