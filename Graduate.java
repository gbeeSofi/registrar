
public class Graduate extends Student {

	String thesisTitle = null;
	String thesisAdvisor = null;
	
	
	@Override
	int calculateTuition() {
	
		return 0;
	}
 	// Thesis Title
    public String getThesisTitle() { 
        return thesisTitle; 
    } 
 
    public void setThesisTitle(String thesisTitle) { 
        this.thesisTitle = thesisTitle; 
    }

 	// Thesis Advisor
    public String getThesisAdvisor() { 
        return thesisAdvisor; 
    } 
 
    public void setThesisAdvisor(String thesisAdvisor) { 
        this.thesisAdvisor = thesisAdvisor; 
    }
	
	// update
    public void update(){
    	DAO.updateStudent(this);	
    }


}
