/*
 * 
 * Author: Greg Bee
 * 
 * Date: 2/18/2012
 * 
 * Description: This code handles the details of connecting to the database.
 * 
 * 
 * Revision History:
 * 		
 * 				GLB Created			2/18/2012
 * 
 */



import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;


public class DAO {
	
	
	static View myView;
	static Connection conn = null;
	

	static String myUrl = "jdbc:mysql://localhost/registrar";
	static String myDriver = "org.gjt.mm.mysql.Driver";
	static String database = "";
	static String user = "root";
	static String databasePassword= "System31";

	// Test Data Automated Generation
	static int numOfStudents = 33; // note there will be 3 times this number 1K total
									// for each type of student
	static int randomSeed = 1; // seed value to reproduce the random values
	
	
	
	public DAO(){
			
	}
	
	
	

	// Connect to database
	public static Connection connectToDatabase() {
		

		try {
			Class.forName(myDriver);
			conn = DriverManager.getConnection(myUrl, "root", databasePassword);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "fatal error, unable to make database connection");
			System.exit(0);
		}
		return conn;
	}

	// Close connection to database
	public static boolean closeConnectionToDatabase(Connection conn) {

		try {
			conn.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"error, unable to close database connection");
			return false;
		}
		return true;
	}

	// Create ddl in database
	public static boolean dropStudentTable(Connection conn) {

		String ddlSQL = "DROP TABLE IF EXISTS `student`;";
		// "SET @saved_cs_client = @@character_set_client;
		// "SET character_set_client = utf8;"; ""

		// debug
		//System.out.println(ddlSQL);

		try {
			Statement stmt = conn.createStatement();
			stmt = conn.createStatement();
			stmt.executeUpdate(ddlSQL);
			stmt.close();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Unable to drop Student Table!"
					+ e);
			return false;
		}
		JOptionPane.showMessageDialog(null, "Student Table Dropped!");
		return true;
	}

	// Create ddl in database
	public static boolean createDDL(Connection conn) {

		String ddlSQL = "CREATE TABLE 		`student` ("
				+ "`studentID` 		int(10) 	NOT NULL PRIMARY KEY AUTO_INCREMENT,"
				+ "`firstName` 		varchar(32) 	NOT NULL,"
				+ "`lastName` 		varchar(32) 	NOT NULL,"
				+ "`gpa` 		float(4) 	NOT NULL,"
				+ "`status` 		varchar(32) 	NOT NULL,"
				+ "`mentor` 		varchar(32) 	NOT NULL,"
				+ "`type` 		varchar(32) 	NOT NULL,"
				+ "`level` 		varchar(32) 	NOT NULL,"
				+ "`thesisTitle` 	varchar(128) 	NOT NULL,"
				+ "`thesisAdvisor` 	varchar(64) 	NOT NULL,"
				+ "`company` 		varchar(32) 	NOT NULL" + ");";

		// debug
		//System.out.println(ddlSQL);

		try {
			Statement stmt = conn.createStatement();
			stmt = conn.createStatement();
			stmt.executeUpdate(ddlSQL);
			stmt.close();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Unable to create DDL!" + e);
			return false;
		}
		JOptionPane.showMessageDialog(null, "Student Table Created!");
		return true;
	}

	public static void populateTestData() {
		TestData myTestData = new TestData(randomSeed);

		//JOptionPane.showMessageDialog(null, "This will create 100 Students, press Ok to continue");
		JFrame f = new JFrame("Please wait, creating test data");
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container content = f.getContentPane();
		JProgressBar progressBar = new JProgressBar(0,100);
		f.setSize(400,70);
		content.add(progressBar);
		
		
		
			

			
		
		
		

		  // make the frame half the height and width
		  Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		  //int height = screenSize.height;
		  //int width = screenSize.width;
		 // f.setSize(width/2, height/2);

		  // here's the part where i center the jframe on screen
		  f.setLocationRelativeTo(null);
		  
		  f.setVisible(true);

		for (int i=1; i <= numOfStudents; i++) {
			progressBar.setValue(i*3);
			DAO.insertTestData(DAO.connectToDatabase(),myTestData.generateTestDataUndergraduateFullTime());
			DAO.insertTestData(DAO.connectToDatabase(),myTestData.generateTestDataGraduate());
			DAO.insertTestData(DAO.connectToDatabase(),myTestData.generateTestDataPartTime());
		}
		progressBar.setValue(100);
		DAO.insertTestData(DAO.connectToDatabase(),myTestData.generateTestDataUndergraduateFullTime());
		JOptionPane.showMessageDialog(null, "100 Student Created!");
		f.dispose();
		
	}

	// insert test data into student table
	public static boolean insertTestData(Connection conn, String sql) {

		String insertStatement = sql;

		// debug
	// System.out.println(insertStatement);

		try {
			Statement stmt = conn.createStatement();
			stmt = conn.createStatement();
			stmt.executeUpdate(insertStatement);
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Unable to insert test data"
					+ e);
			return false;
		}

	return true;
	}
	
	
	
	public static void countStudents(Connection conn) {

			try {
				String query = "SELECT count(*) FROM student";
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(query);
				while (rs.next()) {
					
					JOptionPane.showMessageDialog(null, "Number of Students: " + rs.getString(1));
						
				}
				st.close();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Student Count Failed!" + e);
			}
	}
	

	// insert test data into student table
	public static boolean createNewStudent(Connection conn, Student theStudent, String type) {
		
		

		String undergraduate = "INSERT INTO STUDENT " + 
				"(" +
				"firstName," +			// 1
				"lastName," +			// 2
				"gpa," +				// 3
				"status," +				// 4
				"mentor," +				// 5
				"type," +				// 6
				"level," +				// 7
				"thesisTitle," +		// 8
				"thesisAdvisor," +		// 9
				"company" +				// 10
				") "	+			
				"VALUES " + "('"+
				theStudent.getFirstName().toString() 		+ "','" 	+ 		// 1
				theStudent.getLastName().toString() 		+ "'," 		+ 		// 2
				"0.0"		+ ",'" 		+ 											// 3
				""	 		+ "','" 	+ 										// 4
				theStudent.getMentor().toString()		+ "',"		+ 			// 5
				"'"+ type + "'" 	+ ",'" 		+								// 6
				"Freshman"		+								// 7
				"','" 				+											// 8
				"'," 				+											// 9
				"''," 				+ 											// 10
				"'');";


		//System.out.println(undergraduate);
			
		try {
			Statement stmt = conn.createStatement();
			stmt = conn.createStatement();
			stmt.executeUpdate(undergraduate);
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Unable to insert test data" + e);
			return false;
		}

	return true;
	}

	
	// Student Search
	public static boolean searchStudent(Connection conn, Student searchStudent,final View view, boolean byName )  {

			String searchQuery = "";

			final JDialog multipleStudentSearchDialog = new JDialog();
			multipleStudentSearchDialog.setModal(true);
			final JTable table = new JTable(10, 3);
			JScrollPane scrollPane = new JScrollPane(table);
			JButton selectStudent = new JButton("Ok");
			multipleStudentSearchDialog.add(selectStudent);

			multipleStudentSearchDialog.add(scrollPane, BorderLayout.SOUTH);
			multipleStudentSearchDialog.pack();
			
			//System.out.println("TheStudentID: " + searchStudent.getStudentId());

			if (!(byName)) {
				searchQuery = "SELECT * FROM student where studentID = '"
						+ searchStudent.getStudentId() + "'";
			} else {

				searchQuery = "SELECT * FROM student where " + "firstName like  '"
						+ searchStudent.getFirstName() + "%'" + " AND "
						+ "lastName like  '" + searchStudent.getLastName()+ "%"
						+ "'";
			}

			try {
				
				Statement st = DAO.connectToDatabase().createStatement();
				ResultSet rs = st.executeQuery(searchQuery);

				// get the size of the results
				int size = 0;
				if (rs != null) {
					rs.beforeFirst();
					rs.last();
					size = rs.getRow();
				}

				if (size == 0) {
					JOptionPane
							.showMessageDialog(
									multipleStudentSearchDialog,
									"There were no matches!, you can enter any of the following, First Name, Last Name or StudentID, Please, try Again");
					return false;
				}

				rs.beforeFirst();

				int row = 0;
				while (rs.next()) {
					String firstName = rs.getString("firstName");
					table.setValueAt(firstName, row, 0);
					String lastName = rs.getString("lastName");
					table.setValueAt(lastName, row, 1);
					int studentId = rs.getInt("studentId");
					table.setValueAt(studentId, row, 2);
					if (size == 1) {
						Student sStudent =	DAO.getStudent(DAO.connectToDatabase(), studentId);
						view.displayStudent(sStudent);

					}

					row++;
					//System.out.format("%s, %s,\n", firstName, lastName);
				}
				st.close();
				if (size > 1) {

					// handle calculate button
					selectStudent.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							int tempStudentId = getTableCellsStudentId(table);
							view.displayStudent(getStudent(DAO.connectToDatabase(),tempStudentId));
							//JOptionPane.showMessageDialog(null, "Student Id:" + tempStudentId);
							multipleStudentSearchDialog.setTitle("Select Student?");
							multipleStudentSearchDialog.dispose();
						}
					});
					multipleStudentSearchDialog.setTitle("Select student?");
					multipleStudentSearchDialog.add(table, BorderLayout.NORTH);
					multipleStudentSearchDialog.pack();
					multipleStudentSearchDialog.setLocationRelativeTo(null);
					multipleStudentSearchDialog.setVisible(true);

				}
			} catch (Exception e) {
				System.err.println("Got an exception! " + e);
			}

			return true;
		}


	
	// delete Student
	public static void deleteStudent(Student student) {
				
		try {
			Statement st = conn.createStatement();
			String sql = "DELETE FROM student WHERE studentID = " + student.getStudentId();
			int delete = st.executeUpdate(sql);
			if (delete == 1) {
				JOptionPane
				.showMessageDialog(
						null,"Student with id: "+ student.getStudentId() + " has been deleted!");
			} else {
				
			}
		} catch (Exception e) {
			System.err.println("Got an exception! " + e);
		}
	}
	
	// getNextStudent
	public static void getNextStudent(Connection conn, Student student,View view,int index)  {
		
		int theId = 0;
		int theTotalNumberOfRecords = 60; // to DO write SQL to get this

		if (theId > 0 || theId < theTotalNumberOfRecords) { //

			try {
				String query = "SELECT * FROM student where studentID = "+ index;
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(query);
				while (rs.next()) {
					String studentId = rs.getString("studentId");
					String firstName = rs.getString("firstName");
					String lastName = rs.getString("lastName");
					String level = rs.getString("level");
					float gpa = rs.getFloat("gpa");
					String mentor = rs.getString("mentor");
					
					view.setStudentId(studentId);
					view.setStudentFirstName(firstName);
					view.setStudentLastName(lastName);
					view.setStudentGPA(gpa);
					view.setStudentMentor(mentor);
					view.setStudentTypeBanner(level);					
				}
				st.close();
			} catch (Exception e) {
				System.err.println("Got an exception! " + e);
			}
		}
	}

	
	public static Student getStudent(Connection conn,int studentId)  {
		
		Student aStudent = null;
		
		
		int theTotalNumberOfRecords = 100; // TODO write sql to get this number

		if (studentId > 0 || studentId < theTotalNumberOfRecords) { //

			try {
				String query = "SELECT * FROM student where studentID = "+ studentId;
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(query);
				while (rs.next()) {
					int theStudentId = 		rs.getInt		("studentId");		// 0
					String firstName = 		rs.getString	("firstName");		// 1
					String lastName = 		rs.getString	("lastName");		// 2
					float gpa = 			rs.getFloat		("gpa");			// 3
					String status =			rs.getString	("status");			// 4
					String mentor =			rs.getString	("mentor");			// 5
					String type = 			rs.getString	("type");			// 6
					String level = 			rs.getString	("level");			// 7
					String thesisTitle = 	rs.getString	("thesisTitle");	// 8
					String thesisAdvisor = 	rs.getString	("thesisAdvisor");	// 9
					String company = 		rs.getString	("company");		// 10
					
					// UNDERGRADUATE
					if(type.equalsIgnoreCase("Undergraduate")){
						Undergraduate undergraduate = new Undergraduate();
						undergraduate.setStudentId(theStudentId);
						undergraduate.setFirstName(firstName);
						undergraduate.setLastName(lastName);
						undergraduate.setGPA(gpa);
						undergraduate.setMentor(mentor);
						undergraduate.setLevel(level);
						undergraduate.setStatus(status);
						aStudent = undergraduate;
					
					// GRADUATE
					}else if(type.equalsIgnoreCase("Graduate")){
						Graduate graduate = new Graduate();
						graduate.setStudentId(theStudentId);
						graduate.setFirstName(firstName);
						graduate.setLastName(lastName);
						graduate.setGPA(gpa);
						graduate.setMentor(mentor);
						graduate.setStatus(status);
						graduate.setThesisTitle(thesisTitle);
						graduate.setThesisAdvisor(thesisAdvisor);
						aStudent = graduate;
						
					// PART-TIME
					}else if(type.equalsIgnoreCase("PartTime")){
						PartTime parttime = new PartTime();
						parttime.setStudentId(theStudentId);
						parttime.setFirstName(firstName);
						parttime.setLastName(lastName);
						parttime.setGPA(gpa);
						parttime.setMentor(mentor);
						parttime.setStatus(status);
						parttime.setCompany(company);
						aStudent = parttime;
					}else{
						//shouldn't happen
					}
				}
				st.close();
			} catch (Exception e) {
				System.err.println("Got an exception! " + e);
			}
		}
		return aStudent;	
	}
	

	
	
	// called when more than one match in search
	public static int getTableCellsStudentId(JTable table) {
		String studentid;

		studentid = table.getModel().getValueAt(table.getSelectedRow(), 2).toString();
		
		Integer StudentID = new Integer(studentid);
		return StudentID.intValue();
	}





		
		
		




	public static void updateStudent(Student theStudentToUpdate) {

		String updateSQL = "UPDATE STUDENT SET " +
				" firstName = '" + theStudentToUpdate.getFirstName() + "'," +
				" lastName = '" + theStudentToUpdate.getLastName() + "'," + 
				" level = '"+ "level" + "'," +  
				" mentor = '"+ theStudentToUpdate.getMentor() + "'" +  				
				" WHERE studentid = "+ theStudentToUpdate.getStudentId();
		
		//UPDATE Persons
		// SET Address='Nissestien 67', City='Sandnes'
		// WHERE LastName='Tjessem' AND FirstName='Jakob'
		

		//System.out.println(updateSQL);
		
		try {
			PreparedStatement thePreparedStatement = conn.prepareStatement(updateSQL);
			thePreparedStatement.executeUpdate();
			//System.out.println("Student Updated");
			conn.close();
		} catch (SQLException e) {
			System.out.println("Update Failed" + e);
		}
	} 
}
