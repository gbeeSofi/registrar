/*
 * 
 * Author: Greg Bee
 * 
 * Date: 2/18/2012
 * 
 * Description: This code will generate random fictitious names for the student database as well as
 * 				other random data like GPA, and thesis titles.
 * 
 * 
 * Revision History:
 * 		
 * 				GLB Created			2/18/2012
 * 
 */
import java.util.Random;

public class TestData {
	static Random rand;
	
	
	public TestData(int seed){
		 rand = new Random(seed);
	}


	static String[] status = { "Resident", "nonResident" };

	static String[] thesis1 = { "The", "A", "How", "Be", "Smart",
			"Generation of", "Placing", "Large", "Small", "Building",
			"Defining", "Bonding", "Instructing", "Political", "Election",
			"Planning", "Addition of", "Subtraction of", "Dividing of",
			"Multiplication of", "Sizing", "Distribution of", };

	static String[] thesis2 = { "Space Time Continuum", "Resistance", "Electrons",
			"Hyrdo Flow", "Smart", "Generation of", "States",
			"Relational Databases", "Coding", "Object Orientation and Mathmatical Models",
			"Direct Current", "Alternating Current", "Semiconductors",
			"Arrays", "Collections in String Theory", "Teams of Accelerators ", "People of Mars", "World Web Web Abtracts", "Channels",
			"Adventures", "Margins", "Frequency", };

	static String[] givenNamesMale = { "JEFFREY", "DAVID", "DANIEL", "RUSSELL",
			"JOHN", "DAVID", "PIETER", "JEFF", "CHRISTOPHE", "HARRIS",
			"BRADLEY", "BOB", "DAVID", "DAN", "JUSTIN", "WILLIAM", "ANELISA",
			"REX", "KATHRYN", "VERONICA", "WILLIAM", "SCOTT", };

	static String[] givenNamesFemale = { "SANDRA", "REGINA", "ANGELINE",
			"CAROL", "DOREEN", "ANITA", "CHRIS", "SHARON", "DARLA", "RUTH",
			"CYNTHIA", "DONNA", "ANNA", "BETH", "MEGAN", "JENNIFER", "ANELISA",
			"SUSAN", "KATHRYN", "VERONICA", "SUE", "JACKIE", "RAJANI" };

	static String[] firstNames = { "JEFFREY", "DAVID", "DANIEL", "RUSSELL",
			"JOHN", "DAVID", "PIETER", "JEFF", "CHRISTOPHE", "HARRIS",
			"BRADLEY", "KATHY", "DAVID", "DOROTHY", "JUSTIN", "WILLIAM",
			"ANELISA", "REX", "KATHRYN", "VERONICA", "WILLIAM", "SCOTT",
			"RYAN", "LISA", "LISE", "LANS", "AUSTIN", "ANGELINE", "BRANNON",
			"GLORIA", "SHARON", "DAVID", "RICHARD", "AZER", "MARK", "JOHN",
			"GARY", "DANIEL", "MICHAEL", "DANE", "CAROLINE", "DOUGLAS",
			"DAVID", "JARED", "DAVID", "SCOT", "CAROL", "JIA", "ROBERT",
			"ANITA", "MICHAEL", "MARY", "BRIAN", "IVY", "MICHAEL", "ROBERT",
			"KUOYEN", "PHILLIP", "NATHAN", "RICHARD", "BRUCE", "JAMES",
			"MARCIA", "CAMERON", "JARED", "MARVIN", "NATHAN", "RALPH",
			"RICHARD", "MICHAEL", "JON", "TROY", "MARK", "JOHN", "TERRY",
			"CRAIG", "LANDON", "FLOYD", "WELDEN", "STUART", "JEAN", "RANDELL",
			"BART", "DOREEN", "GRANT", "MICHAEL", "REGINA", "BERT", "JAMES",
			"ELISHA", "MICHAEL", "CHRIS", "PENNY", "JEANNINE", "YVONNE",
			"RICHARD", "RAYMOND", "FRANK", "GEORGE", "REX", "JOHN", "ANIL",
			"MILTON", "BILL", "HOLLEE", "JOYCE", "DALE", "DAVID", "JOHNATHAN",
			"WENDELL", "DAVID", "GAYLEEN", "JOEL", "PAUL", "RAYMOND",
			"MICHAEL", "JAY", "AUGUST", "LDEANE", "FINDLAY", "SHAWN", "ROBERT",
			"JASON", "SUNNY", "SANDRA", "SHANE", "KELLY", "ROBERT", "JAYCEEN",
			"RONALD", "MICHELLE", };

	static String[] lastNames = { "JACK", "BOWMAN", "CABRERA", "MCCABE",
			"WEBB", "LAWSON", "REBER", "LANIER", "CHISUM", "FOWLER", "HATCH",
			"COMPTON", "ZANANIRI", "MAHONEY", "DOUGLAS", "OSTRENGER",
			"JEPPSEN", "ULASEWICZ", "DERR", "HUSSONG", "AUCLAIR", "REALTY",
			"LIVINGSTON", "GUTIERREZ", "LYON", "TIEDEMAN", "WATSON", "CASDEN",
			"SHARP", "PARDICK", "JONES", "SANCHEZ", "AVERY", "DOUVILLE",
			"LAMB", "CHASE", "MCCANN", "DAVIDSON", "THOMPSON", "PEELER",
			"LOPEZ", "SWANSON", "PRICE", "TSAGRINOS", "LEITCH", "HOWARD",
			"FINLEY", "HUANG", "KRUG", "HANNUM", "SIWIEC", "CHILDRESS",
			"MITCHELL", "TOURS", "CLAUSSEN", "DONATO", "THOMAS", "YOUNG",
			"KVOOL", "FRESEMAN", "WEBB", "PACKER", "SPENCER", "DOYLE",
			"DUNPHY", "CAUDLE", "AERY", "SHEEHAN", "NELSON", "MARTIN",
			"WILCOX", "ZIHLMANN", "GOLDBERG", "BURGESS", "WILLIAMS", "ARIAS",
			"JANES", "SPOTTS", "TOLSON", "ADAMS", "ASKIN", "NAKAGAWA",
			"CHRISTENSEN", "BREAULT", "SHEEHAN", "BEAM", "TRAVELL", "HOPPS",
			"VYENIELO", "CARTER", "BRAMBLE", "AYERS", "TRUMBO", "IVERSON",
			"ARENCIBIA", "ARCHER", "VALDES", "MIYAZAKI", "FRISINA", "RIVERA",
			"BARRY", "CARROLL", "WEAR", "BERNARDI", "GODT", "PHONES", "SKELLY",
			"HOBBS", "KHIANI", "ZIMMERMAN", "DOOLERS", "WASSERMAN", "MAGGERT",
			"MIRANDA", "ORNE", "GUZMAN", "KEVORKIAN", "SLONE", "TRAIL",
			"SHERMAN", "JACKSON", "NEILSON", "ESPINOZA", "BELL", "ROMAN",
			"OVERSEN", "GONZALEZ", "SHUGARMAN", "TALKINGTON", "JOHNSON",
			"THOMAS", "BALINT", "HATLEY", "ATHERTON", "SIMPSON", "MOLCHAN",
			"SAPONARO", "WINTHROP", "GUYON", "MITCHELL", "BLACKHURST",
			"STROHL", "BUSCH", "BUCK", "ALLRED", "PETTY", "WEBER", "MARTINI",
			"PETERSON", "SUAREZ", "WATTERS", "DIESON", "VINH", "CARVILLE",
			"HENDERSON", "KAPLAN", "YAN", "GILES", "SMELTZER", "DROLLINGER",
			"SMITH", "BRAGIEL", "WILLIAMS", "ZOLTON", "TRAMMEL", "FINK",
			"GRIGGS", "CLARK", "RAGABO", "BLIZZARD", "BELMARES", "IRBY",
			"COWLEY", "BEATTY", "GRAYSON", "SHERIDAN", "MUCK", "PAMPLONA",
			"YATES", "ARIAS", "BLAZEWICH", "FREY", "MCQUAID", "BOSWORTH",
			"JOHLFS", "JACOBS", "APPLEYARD", "CAMPBELL", "MCCARTHY", "YUST",
			"STEVENSON", "ORSATTI", "RAICHLE", "TEAL", "STEIDEL", "BECKER",
			"DREW", "COSTELLO", "CURTIS", "LIPPON", "CARVER", "FRAZEE", "VARA",
			"MILLER", "JACKSON", "LEFFORGE", "BAILEY", "MARTIN", "BLASCHE",
			"CALLAWAY", "LERNER", "CARLL", "MOON", "KELLY", "LOMBARDO",
			"FEGERT", "METZKER", "ARCILA", "ABRAO", "METZ", "DAY", "PUERNER",
			"EGGER", "DEGLMAN", "SPERLING", "OLSEN", "MEREDITH", "MERKER",
			"THOMPSON", "KAELIN", "BLANCHFIELD", "TODARELLO", "BOWMAN",
			"MCEWAN", "GARCIA", "RANCH", "WOODARD", "PRATT", "MORENO",
			"CAPLINGER", "FOGARTY", "NEWMAN", "KIMPO", "NASH", "WARNER",
			"JOHNSON", "EALY", "FITZPATRICK", "OLENSLAGER", "BATES", "SHAFFER",
			"GENIO", "BURRIS", "MCGUINNESS", "MORENO", "TRAN", "CLARK",
			"CRUME", "MCDONALD", "VOORHEES", "NEWMAN", "STUART", "NASH",
			"BILLINGS", "LEGRAND", "NOWLIN", "GORDON", "HAYES", "RADOVANOVIC",
			"KWIATKOWSKI", "SNOW", "SYLVESTER", "CAMPBELL", "HAASE", "HAIGHT",
			"CULBERT", "RUBINO", "MURRAY", "GRINDE", "ASHWORTH", "ROBINSON",
			"GALLEGOS", "POULIOT", "HARRIS", "RADER", "ROSS", "RUDOLPH",
			"BROWN", "LIND", "OBANNON", "NORTON", "HALL", "GIFFORD", "LUELL",
			"GALAGAN","KAKARLA" };

	static String[] company = { "Litronic Industries", "Meridian Products",
			"D & M Plywood Inc", "Metropolitan Elevator Co",
			"Technology Services", "Century 21 Keewaydin Prop",
			"Beringhause", "Sea Port Record One Stop Inc",
			"Kpff Consulting Engineers", "Albers Technologies Corp",
			"Uchner, David D Esq", "Southern Vermont Surveys",
			"Kahler, Karen T Esq", "National Paper & Envelope Corp",
			"Norton, Robert L Esq", "Waldein Manufacturing",
			"Transit Trading Corp", "Pacific Seating Co",
			"Shapiro, Mark R Esq", "Telair Div Of Teleflex Inc",
			"Audiotek Electronics", "Wrigley, Robert I Esq", "Tax Office",
			"Williams Scotsman", "Davis, Robert L Esq", "Ehrmann, Rolfe F Esq",
			"Chiapete, W Richard Esq", "De Friese Theo & Sons", "Yoshidee Inc",
			"Jd Edwards & Co", "Saunders Appraisal Inc",
			"Jacobs, Brian Realtor", "A B C Lock & Key",
			"Travel Agent Magazine", "Super 8 Motel Temple",
			"Misko, Charles G Esq", "Dream Homes Usa Inc",
			"Lindsays Landing Rv Pk & Mrna", "Smith, Arthur D Esq",
			"Washington Crossing Inn", "Microtel Franchise & Dev Corp",
			"Keyes, Judith Droz Esq", "Manpower Temporary Services",
			"United Waste Systems", "Stell Mortgage Investors", "P C Systems",
			"Savings Bank", "Mooring", "Midwest Undercar Distributors",
			"Midwest Fire Protection", "Bashlin Industries Inc", "Quick Print",
			"Calibron Systems Inc", "Corporate Alternatives Inc",
			"Santa Cruz Title Co", "Fernando Foods Inc", "Pepsi Cola",
			"R & A Associates", "Town Motors", "M & M Mars", "Super 8 Motel",
			"Sunnyvale Travel", "Miller Drugs Inc", "Public Works Department",
			"Precision Steel", "Sears Roebuck And Co", "L & L Builders",
			"Brown & Smith Law Office", "Welsh Company", "House Boat Rentals",
			"Mail Boxes Etc", "Days Inn", "Big Box Mart", "US Mortgage Corp",
			"Ace Plumbing & Heating", "Fun Discovery Inc",
			"Medlin, Charles K Jr", "Buckeye Reserve Title",
			"Oak Brook Capital Corp", "Pepsi Cola Dr Pepper Bottling",
			"Howard Johnson", "Hoover Group Inc", "Ramada Inn",
			"C & C Contractors Inc", "Murtha, Thomas D Esq",
			"Shea, David J Esq", "Khoury Factory Outlet",
			"First American Rlty Assoc Inc", "Midwest Wrecking Company Inc",
			"Andow Personnel Services", "Vasqara & Co", "Hendricks Cpa",
			"Coldwell Banker", "Utility Trailer Sales" };

	static String[] mentors = { "Jared Finley", "Elisha Bragiel",
			"Ryan Johnson", "Milton Nash", "Penny Kimpo", "Michael Swanson",
			"Nathan Jones", "Bruce Grinde", "Jason Stevenson", "Gary Lerner",
			"Michael Blasche", "Jared Beam", "David Hopps", "David Irby",
			"Angeline Ostrenger", "Richard Pardick", "William Carver",
			"Michael Kvool", "Michael Molchan", "Richard Rudolph", "David Buck" };
	
	static String[] advisors = { "David Finley", " Dr. David Kimpo",
		"Dr.David Johnson", "Michael Nash", "Penny Kimpo", "Michael Swanson",
		"Nathan Jones", "Bruce Jones Phd", "Jason Stevenson", "Dr.Michael Gary",
		"Michael Blasche", "Jared Beam MS", "David Hopps", "David Irby",
		"Angeline Williams Phd", "Richard Bragiel", "William Jones",
		"Michael David", "Elisha Molchan", "Richard Rudolph", "David Buck" };

	static String[] gpaWhole = { "1", "2", "3", "4" };

	static String[] gpaPart = { ".1", ".2", ".3", ".4", ".5", ".6", ".7", ".8",".9", ".0" };

	static String[] level = { "Freshman", "Sophomore", "Junior", "Senior" };



	public String generateTestDataUndergraduateFullTime() {

		String undergradFullTime = "";

		undergradFullTime = "INSERT INTO STUDENT " + 
				"(" +
				"firstName," +			// 1
				"lastName," +			// 2
				"gpa," +				// 3
				"status," +				// 4
				"mentor," +				// 5
				"type," +				// 6
				"level," +				// 7
				"thesisTitle," +		// 8
				"thesisAdvisor," +		// 9
				"company" +				// 10
				") "	+			
				"VALUES " + "('"+
				getFirstName() 		+ "','" 	+ 		// 1
				getLastName() 		+ "'," 		+ 		// 2
				generateGPA()		+ ",'" 		+ 		// 3
				getStatus() 		+ "','" 	+ 		// 4
				getMentor() 		+ "',"		+ 		// 5
				"'undergraduate'" 	+ ",'" 		+		// 6
				getLevel()			+					// 7
				"','" 				+					// 8
				"'," 				+					// 9
				"''," 				+ 					// 10
				"'');";

		return (undergradFullTime);
	}



	// Undergraduate Part-Time
	public String generateTestDataGraduate() {
		
		String graduate = "";

		graduate = "INSERT INTO STUDENT " + 
				"(" +
				"firstName," +			// 1
				"lastName," +			// 2
				"gpa," +				// 3
				"status," +				// 4
				"mentor," +				// 5
				"type," +				// 6
				"level," +				// 7
				"thesisTitle," +		// 8
				"thesisAdvisor," +		// 9
				"company" +				// 10				") "				
				") VALUES " + "('"+
				getFirstName() 		+ "','" 	+ 		// 1
				getLastName() 		+ "'," 		+ 		// 2
				generateGPA()		+ ",'" 		+ 		// 3
				getStatus() 		+ "','" 	+ 		// 4
				getMentor() 		+ "',"		+ 		// 5
				"'graduate'" 	+ ",'" 		+			// 6
				"','"		+							// 7
				getThesisTitle() 		+"','"+				// 8
				getThesisAdvisor() 		+ 					// 9
				"','" 				+ 					// 10
				"');";									

		return (graduate);
	}

	public String generateTestDataPartTime() {

		String parttime = "";

		parttime = "INSERT INTO STUDENT " + 
				"(" +
				"firstName," +			// 1
				"lastName," +			// 2
				"gpa," +				// 3
				"status," +				// 4
				"mentor," +				// 5
				"type," +				// 6
				"level," +				// 7
				"thesisTitle," +		// 8
				"thesisAdvisor," +		// 9
				"company" +				// 10				") "				
				") VALUES " + "('"+
				getFirstName() 		+ "','" 	+ 		// 1
				getLastName() 		+ "'," 		+ 		// 2
				generateGPA()		+ ",'" 		+ 		// 3
				getStatus() 		+ "','" 	+ 		// 4
				getMentor() 		+ "',"		+ 		// 5
				"'parttime'" 	+ ",'" 		+		// 6
				"','" 				+					// 8
				"','" 				+					// 8
				"','" 				+ 					// 10
				getCompany() 		+ "');";

		return (parttime);

	}

	static public String getFirstName() {

		int i = rand.nextInt(firstNames.length);
		String firstName = firstNames[i].toLowerCase();
		firstName = firstName.substring(0, 1).toUpperCase()
				+ firstName.substring(1);
		return firstName;

	}

	static public String getMentor() {

		String Mentor = "";
		int i = rand.nextInt(mentors.length);
		Mentor = mentors[i].toUpperCase();
		return Mentor;
	}

	static public String getThesisAdvisor() {

		String theAdvisor = "";
		int i = rand.nextInt(advisors.length);
		theAdvisor = advisors[i].toUpperCase();
		return theAdvisor;
	}
	
	static public String getStatus() {

		String statusType = "";
		int i = rand.nextInt(status.length);
		statusType = status[i].toUpperCase();
		return statusType;

	}

	static public String getLastName() {

		int i = rand.nextInt(lastNames.length);
		String lastName = lastNames[i].toLowerCase();
		lastName = lastName.substring(0, 1).toUpperCase()
				+ lastName.substring(1);

		return lastName;

	}

	static public String getCompany() {

		int i = rand.nextInt(company.length);
		String companyName = company[i].toLowerCase();
		companyName = companyName.substring(0, 1).toUpperCase()
				+ companyName.substring(1);

		return companyName;

	}
	
	static public String getLevel() {

		int i = rand.nextInt(level.length);
		String theLevel = level[i].toLowerCase();


		return theLevel;

	}


	static public String getThesisTitle() {

		String thesisTitle;
		int i = rand.nextInt(thesis1.length);
		String part1 = thesis1[i].toLowerCase();
		int x = rand.nextInt(thesis2.length);
		String part2 = thesis2[x].toLowerCase();
		part1 = part1.substring(0, 1).toUpperCase() + part1.substring(1);
		part2 = part2.substring(0, 1).toUpperCase() + part2.substring(1);
		thesisTitle = part1 + " " + part2;

		return thesisTitle;

	}

	static public float generateGPA() {

		Float theGPA = 1.0f;
		float theGPAPart = .0f;
		int i = rand.nextInt(gpaWhole.length);
		String gpaGenerated = gpaWhole[i].toLowerCase();
		int x = rand.nextInt(gpaPart.length);
		String gpaGeneratedPart = gpaPart[x].toLowerCase();
		theGPA = Float.valueOf(gpaGenerated);
		theGPAPart = Float.valueOf(gpaGeneratedPart);

		if (theGPA + theGPAPart > 4.0) {
			theGPA = 4.0f;
		} else {
			theGPA += theGPAPart;
		}

		return theGPA;
	}
}
