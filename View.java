/*
 * 
 * AUTHOR: Greg Bee
 *
 * DATE: 2/19/2012
 *
 * DESCRIPTION:  
 *
 * CHANGE HISTORY:

 
 
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class View {
	
	 int studentIndex = 0;
			 
	static int windowWidth = 600;
	static int windowHeight = 640;
	
	Dimension screenSize;
	int heightSrcn;
	int widtSrcn;

	static JFrame theFrame = new JFrame();
	static JPanel thePanel = new JPanel();
	static JPanel theSearchPanel = new JPanel();
	static JPanel theStudentPanel = new JPanel();
	
	
	JMenuItem undergraduateStudentMenuItem = new JMenuItem("Undergraduate");
	JMenuItem graduateStudentMenuItem = new JMenuItem("Graduate");
	JMenuItem parttimeStudentMenuItem = new JMenuItem("Part-Time");

	JTextField textBanner = new JTextField();
	
	public JTextField textFieldSearchIdStudent = new JTextField();
	public JTextField textFieldSearchFirstNameStudent = new JTextField();
	public JTextField textFieldSearchLastNameStudent = new JTextField();

	JTextField textFieldStudentIdStudent = new JTextField();
	JTextField textFieldFirstNameStudent = new JTextField();
	JTextField textFieldLastNameStudent = new JTextField();
	JTextField textFieldStudentGPA = new JTextField();
	JTextField textFieldStudentMentor = new JTextField();
	
	JTextField textFieldStudentLevel = new JTextField();
	JTextField textFieldStudentThesisAdvisor = new JTextField();
	JTextField textFieldStudentThesisTitle = new JTextField();
	JTextField texFieldStudentCompany = new JTextField();
	
	static JTextField jtextfieldCreateStudentFirstName = new JTextField();
	static JTextField jtextfieldCreateStudentLastName = new JTextField();
	static JTextField jtextfieldCreateStudentMentor = new JTextField();
	static JTextField jtextfieldCreateStudentLevel = new JTextField();
	
	String createStudentFirstName;
	String createStudentLastName;
	String createStudentMentor;
	String createStudentLevel;
	
	JButton btnSearch;
	JButton btnForward;
	JButton btnBackRead;
	JButton btnDelete;
	JButton btnUpdate;
	JButton btnForwardRead;
	


	/////////////////////////////
	// CONSTRUCTOR
	public View() {


	}

	/////////////////////////////
	// MAIN FRAME
	public void buildWindow() {
		theFrame = new JFrame("Student Database");
		theFrame = new JFrame("KFT1 Student Database");
		theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		theFrame.setSize(windowWidth, windowHeight);
		
		theFrame.setResizable(false);
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		  
			// center frame on screen
			screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			heightSrcn = screenSize.height;
			widtSrcn = screenSize.width;
			theFrame.setLocation(200, 100);
			theFrame.setVisible(true);

		
		
	}

	
	/////////////////////////////
	// SEARCH PANEL - search for students
	public void buildSearchPanel() {

		int btnWidth = 80;
		int leftOffSet = 15;
		int btnHeight = 30;
		int y = 70;
		int width = 170;
		int height = 25;

		btnSearch = new JButton("Search Students");

		thePanel.setLayout(null);
		thePanel.setBounds(10, 60, 555, 170);

		theSearchPanel.setLayout(null);
		theSearchPanel.setBounds(10, 60, 555, 170);
		theSearchPanel.setBorder(BorderFactory.createTitledBorder("Search"));

		theSearchPanel.setBackground(Color.white);
		thePanel.add(theSearchPanel);
		btnSearch.setBounds(220, 130, btnWidth*2, btnHeight);
		theSearchPanel.add(btnSearch);
		// variables for top labels and text fields

		
		
		JLabel jLabelStudentIdSearch = new JLabel("Student Id:");
		theSearchPanel.add(jLabelStudentIdSearch);
		jLabelStudentIdSearch.setBounds(leftOffSet, y - 40, width, height);
		textFieldSearchIdStudent = new JTextField("");
		theSearchPanel.add(textFieldSearchIdStudent);
		textFieldSearchIdStudent.setBounds(90, y - 40, width, height);

		
		JLabel jLabelFirstNameSearch = new JLabel("First Name:");
		theSearchPanel.add(jLabelFirstNameSearch);
		jLabelFirstNameSearch.setBounds(leftOffSet, y, width, height);
		textFieldSearchFirstNameStudent = new JTextField("");
		theSearchPanel.add(textFieldSearchFirstNameStudent);
		textFieldSearchFirstNameStudent.setBounds(90, y, width, height);
		
		
		JLabel jLabelLastNameSearch = new JLabel("Last Name:");
		theSearchPanel.add(jLabelLastNameSearch);
		jLabelLastNameSearch.setBounds(290, y, width, height);
		// jLabelLastNameSearch.setForeground(Color.red.darker());
		textFieldSearchLastNameStudent = new JTextField("");
		theSearchPanel.add(textFieldSearchLastNameStudent);
		textFieldSearchLastNameStudent.setBounds(360, y, width, height);


		// make the enter button the default
		JRootPane theRootPane = theFrame.getRootPane();
		theRootPane.setDefaultButton(btnSearch);


		theFrame.add(thePanel);
		thePanel.add(theSearchPanel);
	}
	
	
	
		/////////////////////////////
		// STUDENT PANEL - where students are displayed
		public void buildStudentPanel() {

			int btnWidth = (windowWidth - 49) / 4;
			int leftOffSet = 15;
			int btnHeight = 30;
			int y = 120;
			int width = 170;
			int height = 25;
			
			
			theStudentPanel = new JPanel();
			theStudentPanel.setLayout(null);
			theStudentPanel.setBounds(leftOffSet, 250, 555, 330);
			theStudentPanel.setBorder(BorderFactory.createTitledBorder("Student"));
			// theStudentPanel.setBorder(null);
			// theStudentPanel.setOpaque(false);
			// theStudentPanel.setBackground(Color.LIGHT_GRAY);
			thePanel.add(theStudentPanel);
			
			
			JLabel jLabelFirstNameStudent = new JLabel("First Name:");
			theStudentPanel.add(jLabelFirstNameStudent);
			jLabelFirstNameStudent.setBounds(leftOffSet, y, width, height);
			textFieldFirstNameStudent = new JTextField("");
			
			
			theStudentPanel.add(textFieldFirstNameStudent);
			textFieldFirstNameStudent.setBounds(90, y, width, height);
			JLabel jLabelLastNameStudent = new JLabel("Last Name:");
			theStudentPanel.add(jLabelLastNameStudent);
			
			
			jLabelLastNameStudent.setBounds(290, y, width, height);
			// jLabelLastNameSearch.setForeground(Color.red.darker());
			textFieldLastNameStudent = new JTextField("");
			theStudentPanel.add(textFieldLastNameStudent);
			textFieldLastNameStudent.setBounds(360, y, width, height);
			
			JLabel jLabelStudentIdStudent = new JLabel("Student Id:");
			theStudentPanel.add(jLabelStudentIdStudent);
			jLabelStudentIdStudent.setBounds(leftOffSet, y - 40, width, height);
			textFieldStudentIdStudent = new JTextField("");
			theStudentPanel.add(textFieldStudentIdStudent);
			textFieldStudentIdStudent.setBounds(90, y - 40, width, height);

			JLabel jLabelStudentGPA = new JLabel("GPA: ");
			jLabelStudentGPA.setBounds(290, y - 40, width, height);
			theStudentPanel.add(jLabelStudentGPA);
			textFieldStudentGPA = new JTextField("");
			theStudentPanel.add(textFieldStudentGPA);
			textFieldStudentGPA.setBounds(360, y-40, width, height);

			JLabel jLabelStudentMentor = new JLabel("Mentor: ");
			jLabelStudentMentor.setBounds(leftOffSet, y+40, width, height);
			theStudentPanel.add(jLabelStudentMentor);
			textFieldStudentMentor = new JTextField("");
			theStudentPanel.add(textFieldStudentMentor);
			textFieldStudentMentor.setBounds(90, y+40, width, height);
			
			
			
			JLabel jLabelLevel = new JLabel("Level: ");
			jLabelLevel.setBounds(290, y + 40, width, height);
			theStudentPanel.add(jLabelLevel);
			textFieldStudentLevel = new JTextField("");
			theStudentPanel.add(textFieldStudentLevel);
			textFieldStudentLevel.setBounds(360, y+40, width, height);

			
			JLabel jLabelStudentThesisAdvisor = new JLabel("Advisor: ");
			jLabelStudentThesisAdvisor.setBounds(leftOffSet, y+80, width, height);
			theStudentPanel.add(jLabelStudentThesisAdvisor);
			textFieldStudentThesisAdvisor = new JTextField("");
			theStudentPanel.add(textFieldStudentThesisAdvisor);
			textFieldStudentThesisAdvisor.setBounds(90, y+80, width, height);

			
			JLabel jLabelStudentThesisTitle = new JLabel("Thesis Title: ");
			jLabelStudentThesisTitle.setBounds(leftOffSet, y+120, width, height);
			theStudentPanel.add(jLabelStudentThesisTitle);
			textFieldStudentThesisTitle = new JTextField("");
			theStudentPanel.add(textFieldStudentThesisTitle);
			textFieldStudentThesisTitle.setBounds(90, y+120, width+270, height);

			
			JLabel jLabelStudentCompany = new JLabel("Company: ");
			jLabelStudentCompany.setBounds(290, y+80, width, height);
			theStudentPanel.add(jLabelStudentCompany);
			texFieldStudentCompany = new JTextField("");
			theStudentPanel.add(texFieldStudentCompany);
			texFieldStudentCompany.setBounds(360, y+80, width, height);


			btnBackRead = new JButton("<< Back");
			btnDelete = new JButton("Delete");
			btnUpdate = new JButton("Update");
			btnForwardRead = new JButton("Forward >>");

			theStudentPanel.setLayout(null);
			theStudentPanel.setBounds(leftOffSet, 250, 555, 330);
			theStudentPanel.setBorder(BorderFactory.createTitledBorder("Student"));

			theStudentPanel.setBackground(Color.lightGray);

	

			theStudentPanel.add(btnBackRead);
			theStudentPanel.add(btnDelete);
			theStudentPanel.add(btnUpdate);
			theStudentPanel.add(btnForwardRead);

			//thePanel.setLayout(null);
			//thePanel.setBounds(10, 60, 555, 170);

			theStudentPanel.add(btnBackRead);
			theStudentPanel.add(btnDelete);
			theStudentPanel.add(btnUpdate);
			theStudentPanel.add(btnForwardRead);

			btnBackRead.setBounds(3, 300, btnWidth, btnHeight);
			btnDelete.setBounds(3 + btnWidth, 300, btnWidth, btnHeight);
			btnUpdate.setBounds(3 + 2 * btnWidth, 300, btnWidth, btnHeight);
			btnForwardRead.setBounds(3 + 3 * btnWidth, 300, btnWidth, btnHeight);

	

			// default button for search, doesn't work
			//btnSearch.registerKeyboardAction(null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0,false), JComponent.WHEN_FOCUSED);

	

			theFrame.add(thePanel);
			thePanel.add(theStudentPanel);
			theFrame.setVisible(true);
			thePanel.setVisible(true);
			theStudentPanel.setVisible(true);
			

		}
	
	
	

	public void buildMenuBar() {

		JMenuBar menuBar = new JMenuBar();

		menuBar.setBounds(10, 15, 400, 29);
		menuBar.setBounds(0, 1, windowWidth, 30);

		thePanel.add(menuBar);

		JMenu fileMenu = new JMenu("File");
		JMenu newStudentMenu = new JMenu("New Student");

		JMenu editMenu = new JMenu("Edit");
		JMenu tuitionMenu = new JMenu("Tuition");
		JMenu helpMenu = new JMenu("Help");

		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(tuitionMenu);
		menuBar.add(helpMenu);

		// All Menu Items
		JMenuItem exitMenuItem = new JMenuItem("Quit");
		JMenuItem cut = new JMenuItem("Cut");
		JMenuItem copy = new JMenuItem("Copy");
		JMenuItem paste = new JMenuItem("Paste");
		cut.setEnabled(false);
		copy.setEnabled(false);
		paste.setEnabled(false);
		
		JMenuItem tuitiontCalculate = new JMenuItem("Calculate...");

		JMenuItem dropStudentTableMenuItem = new JMenuItem("1st - Drop Student Table (DDL)");
		JMenuItem createStudentTableMenuItem = new JMenuItem("2nd - Create Student Table (DDL)");
		JMenuItem populateTestDataMenuItem = new JMenuItem("3rd - Populate Test Data (DML)");
		JMenuItem countTestDataMenuItem = new JMenuItem("4rd - How Many Student Are There? (sql count)");
		JMenuItem aboutMenuItem = new JMenuItem("About");

		fileMenu.add(newStudentMenu);
		fileMenu.add(exitMenuItem);

		newStudentMenu.add(undergraduateStudentMenuItem);
		newStudentMenu.add(graduateStudentMenuItem);
		newStudentMenu.add(parttimeStudentMenuItem);
		
		



   

		// Open Create Student Menus with Alt F1,F2,F3
		undergraduateStudentMenuItem.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_F1, ActionEvent.ALT_MASK));
		graduateStudentMenuItem.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_F2, ActionEvent.ALT_MASK));
		parttimeStudentMenuItem.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_F3, ActionEvent.ALT_MASK));

		editMenu.add(cut);
		editMenu.add(copy);
		editMenu.add(paste);
		editMenu.addSeparator();

		tuitionMenu.add(tuitiontCalculate);

		helpMenu.add(aboutMenuItem);
		helpMenu.add(dropStudentTableMenuItem);
		helpMenu.add(createStudentTableMenuItem);
		helpMenu.add(populateTestDataMenuItem);
		helpMenu.add(countTestDataMenuItem);
		

		
		// drop Student table and recreate it
		tuitiontCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int creditHoursUndergrad = 15;
				int creditHoursGrad = 12;
				int creditHoursPartime = 8;
				float tuition = 0.0f;
				
				Student theStudent = getStudentFromViewDAO();

				
				
				// UNDERGRADUATE CALCULATIONs
				//////////////////////////////////////////
				if (theStudent instanceof Undergraduate){	
			   		if(((Undergraduate) theStudent).getLevel().compareToIgnoreCase("Freshman")==0){
			   			
			   			// Undergraduate Freshman
						if(theStudent.getStatus().compareToIgnoreCase("Resident")==0){
			   				tuition = creditHoursUndergrad * 200;
			   			}else if(theStudent.getStatus().compareToIgnoreCase("nonResident")==0){
			   				tuition = creditHoursUndergrad * 400;
			   			}

			   			JOptionPane.showMessageDialog(null,"Your Freshman Undergraduate tuition is $ " + tuition + "\n" + theStudent.getStatus()+ "\n" + ((Undergraduate) theStudent).getLevel());	
		    		
			   		// Undergraduate Sophomore
			   		}else if(((Undergraduate) theStudent).getLevel().compareToIgnoreCase("Sophomore")==0){
			   			if(theStudent.getStatus().compareToIgnoreCase("Resident")==0){
			   				tuition = creditHoursGrad * 200;
			   			}else if(theStudent.getStatus().compareToIgnoreCase("nonResident")==0){
			   				tuition = creditHoursGrad * 400;
			   			}

			   			JOptionPane.showMessageDialog(null,"Your Sophomore Undergraduate tuition is $ " + tuition + "\n" + theStudent.getStatus()+ "\n" + ((Undergraduate) theStudent).getLevel());	
		    		
			   		// UnderGraduate Junior
			   		}else if(((Undergraduate) theStudent).getLevel().compareToIgnoreCase("junior")==0){
			   			if(theStudent.getStatus().compareToIgnoreCase("Resident")==0){
			   				tuition = creditHoursUndergrad * 200;
			   			}else if(theStudent.getStatus().compareToIgnoreCase("nonResident")==0){
			   				tuition = creditHoursUndergrad * 400;
			   			}

			   			JOptionPane.showMessageDialog(null,"Your Junior Undergraduate tuition is $ " + tuition + "\n" + theStudent.getStatus()+ "\n" + ((Undergraduate) theStudent).getLevel());		
		    		
			   		// UnderGraduate Senior
			   		}else if(((Undergraduate) theStudent).getLevel().compareToIgnoreCase("senior")==0){
			   			if(theStudent.getStatus().compareToIgnoreCase("Resident")==0){
			   				tuition = creditHoursUndergrad * 200;
			   			}else if(theStudent.getStatus().compareToIgnoreCase("nonResident")==0){
			   				tuition = creditHoursUndergrad * 400;
			   			}

			   			JOptionPane.showMessageDialog(null,"Your Senior Undergraduate tuition is $ " + tuition + "\n" + theStudent.getStatus()+ "\n" + ((Undergraduate) theStudent).getLevel());	
		    		}
					
			   	// GRADUATE CALCULATIONs
				//////////////////////////////////////////
				} else if(theStudent instanceof Graduate){

		   			if(theStudent.getStatus().compareToIgnoreCase("Resident")==0){
		   				tuition = creditHoursGrad * 300;
		   			}else if(theStudent.getStatus().compareToIgnoreCase("nonResident")==0){
		   				tuition = creditHoursGrad * 350;
		   			}

		   			JOptionPane.showMessageDialog(null,"Your Graduate tuition is $ " + tuition + "\n" + theStudent.getStatus()+ "\n");		
				   	
		   		// PARTTIME CALCULATIONs
				//////////////////////////////////////////
				} else if(theStudent instanceof PartTime){

		   			if(theStudent.getStatus().compareToIgnoreCase("Resident")==0){
		   				tuition = creditHoursPartime * 250;
		   			}else if(theStudent.getStatus().compareToIgnoreCase("nonResident")==0){
		   				tuition = creditHoursPartime * 450;
		   			}

		   			JOptionPane.showMessageDialog(null,"Your Part Time tuition is $ " + tuition + "\n" + theStudent.getStatus()+ "\n");		
				}
			}
		});

		// drop Student table and recreate it
		createStudentTableMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DAO.createDDL(DAO.connectToDatabase());
			}
		});

		// drop Student table and recreate it
		dropStudentTableMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DAO.dropStudentTable(DAO.connectToDatabase());
			}
		});
		
		// count the number of students in the database
		countTestDataMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DAO.countStudents(DAO.connectToDatabase());
			}
		});
		
		// populate with random test data
		populateTestDataMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DAO.populateTestData();
			}
		});
		
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				aboutMe();
			}
		});
				
		exitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

	}
	
	
	
	//------------------------------
	//UNDERGRADUATE FULL-TIME
	//GRADUATE FULL-TIME
	//CONTINUING EDUCATION-PART-TIME
	
 	//Set Student First Name
    public void setStudentTypeBanner(String studentType) {    	
	
		
		int width = 170;
		int height = 25;
		
		
				

	if (studentType.equalsIgnoreCase("Undergraduate") ) {
		theStudentPanel.add(textBanner);
		textBanner.setBounds(50, 25, width + 300, height);
		textBanner.setText("UNDERGRADUATE");
		textBanner.setFont(null);
		textBanner.setEditable(false);
		textBanner.setBackground(new Color(240,255,255));//Azure
		textBanner.setForeground(Color.blue);
		Font font = new Font("Arial", Font.BOLD, 14);
		textBanner.setHorizontalAlignment(JTextField.CENTER);
		textBanner.setFont(font);
		textBanner.setBorder(BorderFactory.createEtchedBorder());
	} else if (studentType.equalsIgnoreCase("Graduate") ) {
		theStudentPanel.add(textBanner);
		textBanner.setBounds(50, 25, width + 300, height);
		textBanner.setText("GRADUATE");
		textBanner.setFont(null);
		textBanner.setEditable(false);
		textBanner.setBackground( new Color(255,240,245));//Lavender Blush
		textBanner.setForeground(Color.blue);
		Font font = new Font("Arial", Font.BOLD, 14);
		textBanner.setHorizontalAlignment(JTextField.CENTER);
		textBanner.setFont(font);
		textBanner.setBorder(BorderFactory.createEtchedBorder());
	} else if (studentType.equalsIgnoreCase("PartTime") ) {
		theStudentPanel.add(textBanner);
		textBanner.setBounds(50, 25, width + 300, height);
		textBanner.setText("CONTINUING EDUCATION PART-TIME");
		textBanner.setFont(null);
		textBanner.setEditable(false);
		textBanner.setBackground(new Color(250,250,210));//Light Goldenrod Yellow
		textBanner.setForeground(Color.blue);
		Font font = new Font("Arial", Font.BOLD, 14);
		textBanner.setHorizontalAlignment(JTextField.CENTER);
		textBanner.setFont(font);
		textBanner.setBorder(BorderFactory.createEtchedBorder());
	}
    }
	
 	//Set Student First Name
    public void readStudent(final Connection conn, final Student theStudent,View view,int index) { 

    	DAO.getNextStudent(conn, theStudent, view, index);
    	
    }
 
    public Student getStudentFromViewDAO(){
    	
    	Student theStudent = null;
    	
    	if(textFieldStudentIdStudent.getText().length() > 0){
    	
    	Integer studentIdObject = new Integer(textFieldStudentIdStudent.getText());
    	
    	
    	
    	if(textBanner.getText().compareTo("UNDERGRADUATE")==0){
    	
    		Undergraduate undergraduate = new Undergraduate();
    		
    		undergraduate =	(Undergraduate) DAO.getStudent(DAO.connectToDatabase(), studentIdObject.intValue());

    		theStudent = undergraduate;
    		
 
    		
    		
    	
    	}else if(textBanner.getText().compareTo("GRADUATE")==0){
    		Graduate graduate = new Graduate();
        	
    		graduate =	(Graduate) DAO.getStudent(DAO.connectToDatabase(), studentIdObject.intValue());


    		theStudent = graduate;
    		
    	}else if(textBanner.getText().compareTo("CONTINUING EDUCATION PART-TIME")==0){
    		
    		PartTime parttime = new PartTime();
        	
    		parttime =	(PartTime) DAO.getStudent(DAO.connectToDatabase(), studentIdObject.intValue());
    		theStudent = parttime;
    	}
    	
    	}else{
    		JOptionPane.showMessageDialog(null,"Student View is Empty, first Locate a Student!");		
    	}
    	
    
    	
    	
    	
    	
    	return theStudent;
    	
    }

    public Student getStudentFromView(){
    	
    Student theStudent = null;
    	
    if(textFieldStudentIdStudent.getText().length() > 0){
  	
    	
    	if(textBanner.getText().compareTo("UNDERGRADUATE")==0){
    	
    		Undergraduate undergraduate = new Undergraduate();
    		Integer StudentIdStr = new Integer(textFieldStudentIdStudent.getText());
    		undergraduate.setStudentId(StudentIdStr.intValue());
    		undergraduate.setFirstName(textFieldFirstNameStudent.getText());
    		undergraduate.setLastName(textFieldLastNameStudent.getText());
    		undergraduate.setMentor(textFieldStudentMentor.getText());
    		undergraduate.setLevel(textFieldStudentLevel.getText());
    		theStudent = undergraduate;

    	}else if(textBanner.getText().compareTo("GRADUATE")==0){
    		Graduate graduate = new Graduate();
    		Integer StudentIdStr = new Integer(textFieldStudentIdStudent.getText());
    		graduate.setStudentId(StudentIdStr.intValue());       	
    		graduate.setFirstName(textFieldFirstNameStudent.getText());
    		graduate.setLastName(textFieldLastNameStudent.getText());
    		graduate.setMentor(textFieldStudentMentor.getText());
    		graduate.setThesisAdvisor(textFieldFirstNameStudent.getText());
    		graduate.setThesisTitle(textFieldFirstNameStudent.getText());
    		theStudent = graduate;
    		
    	}else if(textBanner.getText().compareTo("CONTINUING EDUCATION PART-TIME")==0){
    		
    		PartTime parttime = new PartTime();
    		Integer StudentIdStr = new Integer(textFieldStudentIdStudent.getText());
    		parttime.setStudentId(StudentIdStr.intValue());        	
    		parttime.setFirstName(textFieldFirstNameStudent.getText());
    		parttime.setLastName(textFieldLastNameStudent.getText());
    		parttime.setMentor(textFieldStudentMentor.getText());
    		parttime.setCompany(texFieldStudentCompany.getText());
    		theStudent = parttime;
    	}
    	
    	}else{
    		JOptionPane.showMessageDialog(null,"Student View is Empty, first Locate a Student!");		
    	}
    	
    	return theStudent;
    	
    }

    
    
 	//Set Student First Name
    public static void createStudent(Connection conn, Student aNewStudent) { 
    }   
 	//Set Student First Name
    public static void newStudentDialog(Student aNewStudent) { 
    	

    	
		int leftOffSet = 60;
		int width = 170;
		int height = 25;
		int btnWidth = 100;
		int btnHeight = 30;
		JButton createNewStudentBtn;
		JButton cancelNewStudentBtn;
		final JFrame createNewStudentFrame;
		final JPanel createNewStudentPanel;
		//JPopupMenu popupMenu = new JPopupMenu("Select Your Mentor...");
		JLabel jlabelStudentType = null;

		
		// create new dialog for Student info
		createNewStudentFrame = new JFrame();
		createNewStudentPanel = new JPanel();
		
		createNewStudentPanel.setLayout(null);
		createNewStudentFrame.setSize(400, 400);
		createNewStudentPanel.setSize(400, 400);
		createNewStudentFrame.add(createNewStudentPanel);
		


		
		createNewStudentBtn = new JButton("Create");
		cancelNewStudentBtn = new JButton("Cancel");
		
		createNewStudentBtn.setBounds(270, 310, btnWidth, btnHeight);
		cancelNewStudentBtn.setBounds(170, 310, btnWidth, btnHeight);
		
		createNewStudentPanel.add(createNewStudentBtn);
		createNewStudentPanel.add(cancelNewStudentBtn);
		
		
		// center the dialog on the screen
		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		final Dimension screenSize = toolkit.getScreenSize();
		final int x = (screenSize.width - createNewStudentFrame.getWidth()) / 2;
		int y = (screenSize.height - createNewStudentFrame.getHeight()) / 2;
		createNewStudentFrame.setLocation(x, y);
		
		// build custom screen layout
		y= 75;
		createNewStudentPanel.setLayout(null);
		
		
		if (aNewStudent instanceof Undergraduate){
			jlabelStudentType = new JLabel("Create New Undergraduate");
			createNewStudentPanel.setBackground( new Color(240,255,255) );//Azure
		}
			
		if (aNewStudent instanceof Graduate){
			jlabelStudentType = new JLabel("Create New Graduate");
			createNewStudentPanel.setBackground( new Color(255,240,245));//Lavender Blush
		}
		
		if (aNewStudent instanceof PartTime){
			jlabelStudentType = new JLabel("Create New Part-time");
			createNewStudentPanel.setBackground( new Color(250,250,210));//Light Goldenrod Yellow
		}
		
		createNewStudentPanel.add(jlabelStudentType);
		jlabelStudentType.setBounds(100, 10, width, height);

		
		// First Name of Student
		JLabel jLabelFirstNameCreateStudent = new JLabel("First Name:");
		createNewStudentPanel.add(jLabelFirstNameCreateStudent);
		jLabelFirstNameCreateStudent.setBounds(leftOffSet, y, width, height);
		createNewStudentPanel.add(jtextfieldCreateStudentFirstName);
		jtextfieldCreateStudentFirstName.setBounds(leftOffSet + 80, y, width, height);


		// Last Name of Student
		JLabel jLabelLastNameCreateStudent = new JLabel("Last Name:");
		createNewStudentPanel.add(jLabelLastNameCreateStudent);
		jLabelLastNameCreateStudent.setBounds(leftOffSet, y + 30, width, height);
		createNewStudentPanel.add(jtextfieldCreateStudentLastName);
		jtextfieldCreateStudentLastName.setBounds(leftOffSet + 80, y + 30, width, height);


		// The Students Level Freshman, Sophomore, Junior, Senior
		JLabel jLabelLevelCreateStudent = new JLabel("Level:");
		createNewStudentPanel.add(jLabelLevelCreateStudent);
		jLabelLevelCreateStudent.setBounds(leftOffSet, y + 60, width, height);
		createNewStudentPanel.add(jtextfieldCreateStudentLevel);
		jtextfieldCreateStudentLevel.setBounds(leftOffSet + 80, y + 60, width, height);

		
		// The Students Mentor
		JLabel jLabelMentorCreateStudent = new JLabel("Mentor:");
		createNewStudentPanel.add(jLabelMentorCreateStudent);
		jLabelMentorCreateStudent.setBounds(leftOffSet, y + 100, width, height);
		createNewStudentPanel.add(jtextfieldCreateStudentMentor);
		jtextfieldCreateStudentMentor.setBounds(leftOffSet + 80, y + 100, width, height);
		
		
		
		
		/* TODO Popup Menu for  Level and Mentor *//*
		popupMenu = new JPopupMenu("Select Your Mentor...");
	
		createNewStudentPanel.add(popupMenu);
		popupMenu.setBounds(leftOffSet + 80, y + 100, width, height);
		
		popupMenu.add(new JMenuItem("Greg Bee"));
		popupMenu.add(new JMenuItem("Cindy Bee"));
		popupMenu.add(new JMenuItem("Betany Bee"));
		
		popupMenu.setVisible(true); 
		    
		// createNewStudentDialog.addMouseListener(new MouseAdapter() {
		   //     public void mouseReleased(MouseEvent e) {
		  //          if (e.isPopupTrigger()) {
		  //          	popupMenu.setLocation(e.getX(), e.getY());
		  //          	popupMenu.setInvoker(popupMenu);
		  //          	popupMenu.setVisible(true);
		  //          }
		 //       }
		//    });
	*/


		


		
		// create a new student
		createNewStudentBtn.addActionListener(new ActionListener() {   
            public void actionPerformed(ActionEvent e) {
            	//String type = "Undergraduate";
            	//DAO.createNewStudent(DAO.connectToDatabase(), this, type);
            	//createNewStudentFrame.dispose();
            	
            	Undergraduate theNewStudent = new Undergraduate();
            	theNewStudent.setFirstName(jtextfieldCreateStudentFirstName.getText());
            	theNewStudent.setLastName(jtextfieldCreateStudentLastName.getText());
            	theNewStudent.setMentor(jtextfieldCreateStudentMentor.getText());
            	theNewStudent.setLevel(jtextfieldCreateStudentLevel.getText());
            	DAO.createNewStudent(DAO.connectToDatabase(),(Undergraduate) theNewStudent, "undergraduate");
            	JOptionPane.showMessageDialog(null, "New Student Created!");
            	createNewStudentFrame.dispose();
            }   
        });
		
		
		// cancel a new student
		cancelNewStudentBtn.addActionListener(new ActionListener() {   
            public void actionPerformed(ActionEvent e) {   
               createNewStudentFrame.dispose();
            }   
        });   


		createNewStudentFrame.setVisible(true);
    }   
    
    
    

	public boolean displayStudent(Student student) {
		boolean success = false;
	
		if(student == null){
			return false;	
		}
		
		
		Float theGPA = new Float(student.getGPA());
		
		textFieldStudentIdStudent.setText("");
		textFieldFirstNameStudent.setText("");
		textFieldLastNameStudent.setText("");
		textFieldStudentGPA.setText("");
		textFieldStudentMentor.setText("");
		textFieldStudentLevel.setText("");
		textFieldStudentLevel.setText("");
		textFieldStudentThesisAdvisor.setText("");
		textFieldStudentThesisTitle.setText("");
		texFieldStudentCompany.setText("");
	
		
		if (student instanceof Undergraduate) {
			Integer theStudentId = new Integer(student.getStudentId());
			textFieldStudentIdStudent.setText(theStudentId.toString());
			textFieldFirstNameStudent.setText(student.getFirstName());
			textFieldLastNameStudent.setText(student.getLastName());
			textFieldStudentGPA.setText(theGPA.toString());
			textFieldStudentMentor.setText(student.getMentor());
			textFieldStudentLevel.setText(((Undergraduate) student).getLevel());
			setStudentTypeBanner("Undergraduate");
		} else if (student instanceof Graduate) {
			Integer theStudentId = new Integer(student.getStudentId());
			textFieldStudentIdStudent.setText(theStudentId.toString());
			textFieldFirstNameStudent.setText(student.getFirstName());
			textFieldLastNameStudent.setText(student.getLastName());
			textFieldStudentGPA.setText(theGPA.toString());
			textFieldStudentMentor.setText(student.getMentor());
			textFieldStudentThesisAdvisor.setText(((Graduate) student).getThesisAdvisor());
			textFieldStudentThesisTitle.setText(((Graduate) student).getThesisTitle());
			setStudentTypeBanner("Graduate");
		} else if (student instanceof PartTime) {
			Integer theStudentId = new Integer(student.getStudentId());
			textFieldStudentIdStudent.setText(theStudentId.toString());
			textFieldFirstNameStudent.setText(student.getFirstName());
			textFieldLastNameStudent.setText(student.getLastName());
			textFieldStudentGPA.setText(theGPA.toString());
			textFieldStudentMentor.setText(student.getMentor());
			texFieldStudentCompany.setText(((PartTime) student).getCompany());
			setStudentTypeBanner("PartTime");
		} else {
			System.out.println("Student not determined,shouldn't happen");
		}
		return success;
	}
    
	
 	// About Application
    public static void aboutMe() { 	
    	
    	final JDialog d = new JDialog(); 
    	d.setSize(350, 245); 
    	final Toolkit toolkit = Toolkit.getDefaultToolkit(); 
    	final Dimension screenSize = toolkit.getScreenSize(); 
    	final int x = (screenSize.width - d.getWidth()) / 2; 
    	final int y = (screenSize.height - d.getHeight()) / 2; 
    	d.setLocation(x, y);     	
    	String gregbeePict = "";
		JLabel fancyLabel = new JLabel(gregbeePict, new ImageIcon("C:/Registrar/src/About.gif"), JLabel.CENTER);
		d.add(fancyLabel);
    	d.setVisible(true); 
    }
	
	
   

 	//Set Student First Name
    public void setStudentId(String studentStudentId) {    	
    	textFieldStudentIdStudent.setText(studentStudentId);
    }
	 
	 
 	//Set Student First Name
    public void setStudentFirstName(String studentFirstName) {    	
    	textFieldFirstNameStudent.setText(studentFirstName);
    }
    
    
 	//Set Student First Name
    public void setStudentLastName(String studentLastName) {    	
    	textFieldLastNameStudent.setText(studentLastName);
    }

 	//Set Student First Name
    public void setStudentGPA(float studentGPA) { 
    	textFieldStudentGPA.setText(Float.toString(studentGPA));
    }
   
 	//Set Student Mentor
    public void setStudentMentor(String Mentor) { 
    	textFieldStudentMentor.setText(Mentor);
    }
    
 	//Search get StudentId
    public String getSearchStudentId() { 
    	return textFieldSearchIdStudent.getText();
    }

 	//Search get First Name
    public String getSearchFirstName() { 
    	return textFieldSearchFirstNameStudent.getText();
    }

 	//Search get First Name
    public String getSearchLastName() { 
    	return textFieldSearchLastNameStudent.getText();
    }

    
 	//Student Panel get StudentId
    public int getStudentId() { 
    	Integer theStudentID = new Integer(textFieldStudentIdStudent.getText().toString());	
    	return theStudentID.intValue();
    }
  
    
    
    
    
    
 	// New Student First Name
    public String getNewStudentFirstName() { 
    	return jtextfieldCreateStudentFirstName.getText();
    }
 	
    // New Student First Name
    public String getNewStudentLastName() { 
    	return jtextfieldCreateStudentLastName.getText();
    }

 	// New Student First Name
    public String getNewStudentMentorName() { 
    	return jtextfieldCreateStudentMentor.getText();
    }

 	// New Student Level Name
    public String getNewStudentLevelName() { 
    	return jtextfieldCreateStudentLevel.getText();
    }


}
