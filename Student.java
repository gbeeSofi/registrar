
public abstract class Student {
	
	
	 private String firstName;
	 private String lastName;
	 private int studentID;
	 private String status;  // resident or nonresident
	 private float gpa;
	 private String mentor;
	 

 abstract int calculateTuition();
	 
	 	//getters & setters
	 
	 
	 	//First Name
	    public String getFirstName() { 
	        return firstName; 
	    } 
	 
	    public void setFirstName(String firstName) { 
	        this.firstName = firstName; 
	    }
	    
	    
	    // Last Name
	    public String getLastName() { 
	        return lastName; 
	    } 
	    
	    public void setLastName(String lastName) { 
	        this.lastName = lastName; 
	    }
	    
	    
	 	// StudentID
	    public int getStudentId() { 
	        return studentID; 
	    } 
	 
	    public void setStudentId(int studentID) { 
	        this.studentID = studentID; 
	    }
	    
	    
	 	// Status resident or nonresident
	    public String getStatus() { 
	        return status; 
	    } 
	 
	    public void setStatus(String status) { 
	        this.status = status; 
	    }
	    
	    
	 	// GPA
	    public float getGPA() { 
	        return gpa; 
	    } 
	 
	    public void setGPA(float gpa) { 
	        this.gpa = gpa; 
	    }
	    
	    // Mentor
		public String getMentor() {
			return mentor;
		}

		public void setMentor(String mentor) {
			this.mentor = mentor;
		}
	   
		}
	    


